// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair
#include <set>

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>
            , Graph // uncomment
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}

    TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(vdA, g);
    set<vertex_descriptor> actual(adj.first, adj.second);

    set<vertex_descriptor> expected;
    expected.insert(vdB);
    expected.insert(vdC);
    
    ASSERT_EQ(expected, actual);}

TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<edge_descriptor, bool> actual = edge(vdB, vdC, g);

    ASSERT_EQ(actual.second, false);}

TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using edge_iterator       = typename TestFixture:: edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor AB = add_edge(vdA, vdB, g).first;
    edge_descriptor AC = add_edge(vdA, vdC, g).first;
    
    pair<edge_iterator, edge_iterator> edge_it = edges(g);
    edge_iterator b = edge_it.first;
    edge_iterator e = edge_it.second;

    ASSERT_NE(b, e);

    edge_descriptor cur = *b;
    ASSERT_EQ(cur, AB);

    ++b;
    cur = *b;
    ASSERT_EQ(cur, AC);}

    TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    
    ASSERT_EQ(num_edges(g), 2);}

    TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    
    ASSERT_EQ(num_vertices(g), 3);}

    TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor AB = add_edge(vdA, vdB, g).first;
    add_edge(vdA, vdC, g);
    
    ASSERT_EQ(source(AB, g), vdA);}

    TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor AB = add_edge(vdA, vdB, g).first;
    add_edge(vdA, vdC, g);
    
    ASSERT_EQ(target(AB, g), vdB);}

    TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    
    ASSERT_EQ(vertex(0, g), vdA);
    ASSERT_EQ(vertex(1, g), vdB);
    ASSERT_EQ(vertex(2, g), vdC);}

TYPED_TEST(GraphFixture, test13) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    add_edge(10, 13, g);

    ASSERT_EQ(vertex(10, g), 10);
    ASSERT_EQ(num_vertices(g), 14);
    ASSERT_EQ(num_edges(g), 1);}

TYPED_TEST(GraphFixture, test14) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    for(int i = 0; i < 1000; ++i){
        add_vertex(g);
    }

    ASSERT_EQ(num_vertices(g), 1001);
    ASSERT_EQ(num_edges(g), 0);}

TYPED_TEST(GraphFixture, test15) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    add_edge(30, 2, g);

    ASSERT_EQ(num_vertices(g), 31);
    ASSERT_EQ(num_edges(g), 1);}

    TYPED_TEST(GraphFixture, test16) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    add_edge(30, 2, g);
    pair<edge_descriptor, bool> e = add_edge(30, 2, g);
    add_edge(30, 2, g);

    ASSERT_EQ(e.second, false);
    ASSERT_EQ(num_vertices(g), 31);
    ASSERT_EQ(num_edges(g), 1);}

    TYPED_TEST(GraphFixture, test17) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    add_edge(30, 2, g);
    add_edge(30, 2, g);
    pair<edge_descriptor, bool> e = add_edge(2, 30, g);

    ASSERT_EQ(e.second, true);
    ASSERT_EQ(num_vertices(g), 31);
    ASSERT_EQ(num_edges(g), 2);}

    TYPED_TEST(GraphFixture, test18) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    pair<edge_descriptor, bool> e1 = add_edge(2, 2, g);
    pair<edge_descriptor, bool> e2 = add_edge(2, 2, g);

    ASSERT_EQ(e1.second, true);
    ASSERT_EQ(e2.second, false);
    ASSERT_EQ(num_vertices(g), 3);
    ASSERT_EQ(num_edges(g), 1);}

    TYPED_TEST(GraphFixture, test19) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vA = add_vertex(g);

    pair<edge_descriptor, bool> e = add_edge(vA, vA, g);

    ASSERT_EQ(e.second, true);
    ASSERT_EQ(source(e.first, g), vA);
    ASSERT_EQ(target(e.first, g), vA);}

    TYPED_TEST(GraphFixture, test20) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vA = add_vertex(g1);

    pair<edge_descriptor, bool> e = add_edge(vA, vA, g1);

    ASSERT_EQ(e.second, true);
    ASSERT_EQ(num_edges(g2), 0);
    ASSERT_EQ(source(e.first, g2), vA);
    ASSERT_EQ(num_edges(g2), 0);
    ASSERT_EQ(target(e.first, g1), vA);}

TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdC, g);
    add_edge(vdB, vdB, g);
    add_edge(vdB, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(vdB, g);
    set<vertex_descriptor> actual(adj.first, adj.second);

    set<vertex_descriptor> expected;
    expected.insert(vdB);
    expected.insert(vdA);
    
    ASSERT_EQ(expected, actual);}

    TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdC, g);
    add_edge(vdB, vdB, g);
    add_edge(vdB, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(vdA, g);
    set<vertex_descriptor> actual(adj.first, adj.second);

    set<vertex_descriptor> expected;
    expected.insert(vdC);
    
    ASSERT_EQ(expected, actual);}

    TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdC, g);
    add_edge(vdB, vdB, g);
    add_edge(vdB, vdA, g);

    pair<edge_descriptor, bool> e = edge(vdA, vdB, g);

    ASSERT_EQ(e.second, false);}

    TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdC, g);
    add_edge(vdB, vdB, g);
    add_edge(vdB, vdA, g);

    pair<edge_descriptor, bool> e = edge(vdB, vdA, g);

    ASSERT_EQ(e.second, true);}

TYPED_TEST(GraphFixture, test25) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> AC = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> BB = add_edge(vdB, vdB, g);
    pair<edge_descriptor, bool> BA = add_edge(vdB, vdA, g);

    pair<edge_descriptor, bool> e = edge(vdB, vdA, g);

    pair<edge_iterator, edge_iterator> edgeList = edges(g);

    set<edge_descriptor> actual(edgeList.first, edgeList.second);

    set<edge_descriptor> expected;
    expected.insert(AC.first);
    expected.insert(BB.first);
    expected.insert(BA.first);

    ASSERT_EQ(actual, expected);}

    TYPED_TEST(GraphFixture, test26) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor unknown = vertex(1, g);

    ASSERT_EQ(unknown, 1);
    ASSERT_EQ(num_vertices(g), 1);}

    TYPED_TEST(GraphFixture, test27) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator  = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> vertexList = vertices(g);

    EXPECT_TRUE(vertexList.first == vertexList.second);}

    TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator  = typename TestFixture::vertex_iterator;

    graph_type g;

    ASSERT_EQ(num_edges(g), 0);}

    TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator  = typename TestFixture::vertex_iterator;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);}

    TYPED_TEST(GraphFixture, test30) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator  = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> verts = vertices(g);

    ASSERT_NE(verts.first, verts.second);

    set<vertex_descriptor> actual(verts.first, verts.second);
    set<vertex_descriptor> expected;
    expected.insert(vdA);
    expected.insert(vdB);
    expected.insert(vdC);
    expected.insert(vdD);

    ASSERT_EQ(actual, expected);}

