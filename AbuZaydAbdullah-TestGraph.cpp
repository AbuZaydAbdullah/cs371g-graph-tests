// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream>  // cout, endl
#include <iterator>  // ostream_iterator
#include <sstream>   // ostringstream
#include <utility>   // pair

#include "boost/graph/adjacency_list.hpp"  // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
  // ------
  // usings
  // ------

  using graph_type = G;
  using vertex_descriptor = typename G::vertex_descriptor;
  using edge_descriptor = typename G::edge_descriptor;
  using vertex_iterator = typename G::vertex_iterator;
  using edge_iterator = typename G::edge_iterator;
  using adjacency_iterator = typename G::adjacency_iterator;
  using vertices_size_type = typename G::vertices_size_type;
  using edges_size_type = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using graph_types =
    Types<boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
          Graph>;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types, );
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, add_edge1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  add_edge(vdA, vdB, g);

  ASSERT_TRUE(num_edges(g) == 1);
}

TYPED_TEST(GraphFixture, add_edge2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdA, vdC, g);

  ASSERT_TRUE(num_edges(g) == 2);
}

TYPED_TEST(GraphFixture, add_edge3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  ASSERT_TRUE(num_edges(g) == 1);
  add_edge(vdA, vdB, g);
  ASSERT_TRUE(num_edges(g) == 1);
}

TYPED_TEST(GraphFixture, add_edge4) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  auto e = add_edge(vdA, vdB, g);
  ASSERT_TRUE(num_edges(g) == 1);
  add_edge(vdA, vdB, g);
  ASSERT_TRUE(e.second == true);
}

TYPED_TEST(GraphFixture, add_edge5) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  ASSERT_TRUE(num_edges(g) == 1);
  auto e = add_edge(vdA, vdB, g);
  ASSERT_TRUE(e.second == false);
}

TYPED_TEST(GraphFixture, add_vertex1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);

  ASSERT_TRUE(num_vertices(g) == 1);
}

TYPED_TEST(GraphFixture, add_vertex2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  ASSERT_TRUE(num_vertices(g) == 2);
}

TYPED_TEST(GraphFixture, add_vertex3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  ASSERT_TRUE(vdA != vdB);
}

TYPED_TEST(GraphFixture, add_vertex4) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  ASSERT_TRUE(num_vertices(g) == 0);
}

TYPED_TEST(GraphFixture, adjacent_vertices1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using adjacency_iterator = typename TestFixture::adjacency_iterator;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);

  auto[b, e] = adjacent_vertices(vdA, g);
  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }

  ASSERT_TRUE(distance == 1);
}

TYPED_TEST(GraphFixture, adjacent_vertices2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using adjacency_iterator = typename TestFixture::adjacency_iterator;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdA, vdC, g);

  auto[b, e] = adjacent_vertices(vdA, g);
  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }

  ASSERT_TRUE(distance == 2);
}

TYPED_TEST(GraphFixture, adjacent_vertices3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using adjacency_iterator = typename TestFixture::adjacency_iterator;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdA, vdC, g);
  add_edge(vdB, vdC, g);

  {
    auto[b, e] = adjacent_vertices(vdA, g);
    size_t distance = 0;

    while (b != e) {
      ++distance;
      ++b;
    }
    ASSERT_TRUE(distance == 2);
  }
  {
    auto[b, e] = adjacent_vertices(vdB, g);
    size_t distance = 0;

    while (b != e) {
      ++distance;
      ++b;
    }
    ASSERT_TRUE(distance == 1);
  }
}

TYPED_TEST(GraphFixture, adjacent_vertices4) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using adjacency_iterator = typename TestFixture::adjacency_iterator;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdA, vdC, g);
  add_edge(vdB, vdA, g);
  add_edge(vdC, vdA, g);

  {
    auto[b, e] = adjacent_vertices(vdA, g);
    size_t distance = 0;

    while (b != e) {
      ++distance;
      ++b;
    }
    ASSERT_TRUE(distance == 2);
  }
  {
    auto[b, e] = adjacent_vertices(vdB, g);
    size_t distance = 0;

    while (b != e) {
      ++distance;
      ++b;
    }
    ASSERT_TRUE(distance == 1);
  }
  {
    auto[b, e] = adjacent_vertices(vdC, g);
    size_t distance = 0;

    while (b != e) {
      ++distance;
      ++b;
    }
    ASSERT_TRUE(distance == 1);
  }
}

TYPED_TEST(GraphFixture, edge1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  auto e = edge(vdA, vdB, g);
  ASSERT_TRUE(e.second == true);
}

TYPED_TEST(GraphFixture, edge2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  auto e = edge(vdA, vdC, g);
  ASSERT_TRUE(e.second == false);
}

TYPED_TEST(GraphFixture, edge3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdB, vdA, g);
  auto e = edge(vdA, vdB, g);
  ASSERT_TRUE(e.second == false);
}

TYPED_TEST(GraphFixture, edges1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdB, vdA, g);
  auto[b, e] = edges(g);
  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }
  ASSERT_TRUE(distance == 1);
}

TYPED_TEST(GraphFixture, edges2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdB, vdA, g);

  auto[b, e] = edges(g);
  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }
  ASSERT_TRUE(distance == 2);
}

TYPED_TEST(GraphFixture, edges3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdA, vdB, g);

  auto[b, e] = edges(g);
  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }
  ASSERT_TRUE(distance == 1);
}

TYPED_TEST(GraphFixture, num_edges1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  add_edge(vdA, vdB, g);

  ASSERT_TRUE(num_edges(g) == 1);
}

TYPED_TEST(GraphFixture, num_edges2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdA, vdC, g);

  ASSERT_TRUE(num_edges(g) == 2);
}

TYPED_TEST(GraphFixture, num_edges3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);
  vertex_descriptor vdC = add_vertex(g);

  add_edge(vdA, vdB, g);
  add_edge(vdB, vdA, g);

  ASSERT_TRUE(num_edges(g) == 2);
}

TYPED_TEST(GraphFixture, num_vertices1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);

  ASSERT_TRUE(num_vertices(g) == 1);
}

TYPED_TEST(GraphFixture, num_vertices2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  ASSERT_TRUE(num_vertices(g) == 2);
}

TYPED_TEST(GraphFixture, num_vertices3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  ASSERT_TRUE(num_vertices(g) == 0);
}

TYPED_TEST(GraphFixture, source1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  auto e = add_edge(vdA, vdB, g);

  ASSERT_TRUE(vdA == source(e.first, g));
}

TYPED_TEST(GraphFixture, source2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  add_edge(vdA, vdB, g);
  auto e = add_edge(vdB, vdA, g);

  ASSERT_TRUE(vdB == source(e.first, g));
}

TYPED_TEST(GraphFixture, target1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  auto e = add_edge(vdA, vdB, g);

  ASSERT_TRUE(vdB == target(e.first, g));
}

TYPED_TEST(GraphFixture, target2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  add_edge(vdA, vdB, g);
  auto e = add_edge(vdB, vdA, g);

  ASSERT_TRUE(vdA == target(e.first, g));
}

TYPED_TEST(GraphFixture, vertex1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);

  ASSERT_TRUE(vdA == vertex(0, g));
}

TYPED_TEST(GraphFixture, vertex2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  ASSERT_TRUE(vdB == vertex(1, g));
}

TYPED_TEST(GraphFixture, vertex3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  ASSERT_TRUE(vdA != vertex(1, g));
}

TYPED_TEST(GraphFixture, vertices1) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);

  auto[b, e] = vertices(g);

  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }

  ASSERT_TRUE(distance == 1);
}

TYPED_TEST(GraphFixture, vertices2) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  vertex_descriptor vdA = add_vertex(g);
  vertex_descriptor vdB = add_vertex(g);

  auto[b, e] = vertices(g);

  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }

  ASSERT_TRUE(distance == 2);
}

TYPED_TEST(GraphFixture, vertices3) {
  using graph_type = typename TestFixture::graph_type;
  using vertex_descriptor = typename TestFixture::vertex_descriptor;
  using edge_descriptor = typename TestFixture::edge_descriptor;

  graph_type g;

  auto[b, e] = vertices(g);

  size_t distance = 0;

  while (b != e) {
    ++distance;
    ++b;
  }

  ASSERT_TRUE(distance == 0);
}